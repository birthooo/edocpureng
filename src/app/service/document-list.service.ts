import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DocumentListService {

    constructor(private http: HttpClient) { }

    DocumentList(token:string, cmuaccount:string, key:string) {
        let requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebView/getEdocRecieveTab?Cmuaccount='+cmuaccount+'&key='+key;
        let headers = new HttpHeaders({'token' : token});
        return this.http.post(requestUrl, '', { headers : headers} );
    }
}