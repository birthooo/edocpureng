import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DocumentDetailService {

    constructor(private http: HttpClient) { }

    DocumentDetail(token:string, docID:string, docsubid:string, cmuaccount:string) {
        let requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebView/getEdocDetail?docID='+docID+'&docsubid='+docsubid+'&cmuaccount='+cmuaccount;
        let headers = new HttpHeaders({'token' : token});
        return this.http.post(requestUrl, '', { headers : headers} );
    }
}