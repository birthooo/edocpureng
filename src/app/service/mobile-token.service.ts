import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class MobileTokenService {

  constructor(private http: HttpClient) { }

  getMobileToken(token:string, cmuaccount:string) {
    let requestUrl = 'https://mobileapi.cmu.ac.th/api/user/useOnetimeToken';
    let headers = new HttpHeaders({'token' : token, 'cmuaccount': cmuaccount});
    return this.http.post(requestUrl, '', { headers : headers} );
  }

}
