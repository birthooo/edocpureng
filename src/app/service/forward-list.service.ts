import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ForwardListService {

    constructor(private http: HttpClient) { }

    getForwardList(token: string, cmuaccount: string, key: string) {
        let requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebView/getprocSEmployeeByName?cmuaccount='+cmuaccount+'&key='+key;
        let headers = new HttpHeaders({'token' : token});
        return this.http.post(requestUrl, '', { headers : headers} );
    }
}