import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DocumentAcceptService {

    constructor(private http: HttpClient) { }

    acceptDocument(token:string, cmuaccount:string, docid:string, docsubid:string) {
        let requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebView/updateAcceptStatus?cmuaccount='+cmuaccount+'&docid='+docid+'&docsubid='+docsubid;
        let headers = new HttpHeaders({'token' : token,'Content-Type' : 'application/json'});
        return this.http.post(requestUrl, '', { headers : headers} );
    }
}