import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export class UserValueService {

  constructor() { }

  setUserValue(token: string, cmuaccount: string) {
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('account', cmuaccount);
    console.log('service SET : ' + sessionStorage.getItem('token') + ' , ' + sessionStorage.getItem('account'));
  }

  getUserValue(): Observable<any> {
    console.log('service GET : ' + sessionStorage.getItem('token') + ' , ' + sessionStorage.getItem('account'));
    return Observable.of(JSON.parse('{ "token":"'+sessionStorage.getItem('token')+'" , "cmuaccount":"'+sessionStorage.getItem('account')+'" }'));
  }
}
