import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DoForwardService {

    constructor(private http: HttpClient) { }

    doForward(action: string, token: string, cmuaccount: string, docid: string, docsubid: string, comment: string, body: string) {
        let requestUrl = '';
        if ( action === "REPLY" ) {
            requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebView/replyEdocMulti?';
        } else {
            requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebView/ForwardEdocMulti?';
        }
        requestUrl += 'docid='+docid+'&docsubid='+docsubid+'&cmuaccount='+cmuaccount+'&comment='+comment;
        let headers = new HttpHeaders({'token' : token});
        return this.http.post(requestUrl, body, { headers : headers} );
    }
}