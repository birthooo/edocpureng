import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class DocumentDeleteService {

    constructor(private http: HttpClient) { }

    deleteDocument(token:string, cmuaccount:string, body:string) {
        let requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebView/delteEdocRecive?cmuaccount='+cmuaccount;
        let headers = new HttpHeaders({'token' : token,'Content-Type' : 'application/json'});
        return this.http.post(requestUrl, body, { headers : headers} );
    }
}