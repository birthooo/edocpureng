import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class EditIsOpenService {

    constructor(private http: HttpClient) { }

    EditIsOpen(token:string, docsubid:string, cmuaccount:string) {
        let requestUrl = 'https://mobileapi.cmu.ac.th/api/EdocWebview/editIsOpen?docsubid='+docsubid+'&cmuaccount='+cmuaccount;
        let headers = new HttpHeaders({'token' : token});
        return this.http.post(requestUrl, '', { headers : headers} );
    }
}