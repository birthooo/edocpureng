import { Component, OnInit, Input } from '@angular/core';
import { DocumentDeleteService } from './../../service/documents-delete.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-edit-doc',
  templateUrl: './edit-doc.component.html',
  styleUrls: ['./edit-doc.component.css']
})
export class EditDocComponent implements OnInit {

  @Input() doucumentList;
  @Input() token: string;
  @Input() cmuaccount: string;
  
  public mobiletoken;
  public selectedArray: Array<number> = [];
  public deleteCallbackStatus;

  constructor(private _documentDelete:DocumentDeleteService) {
  }

  ngOnInit() {
    $('.edit-item-wrapper').click(function() {
      alert(0);
      $(this).attr('src','./assets/img/checked_icon.png');
    });
  }

  deleteDocument() {
    $('app-loading').show();
    var body = JSON.stringify({'params': {'data': this.selectedArray} });
    this._documentDelete.deleteDocument(this.token, this.cmuaccount, body).subscribe(
      data => {
        this.deleteCallbackStatus = data;
        if(this.deleteCallbackStatus.status === "pass"){
          location.reload();
        }
      },
      err => console.error(err),
      () => {}
    );
  }

  closeEdit(){
    $('.dim-background').fadeOut(100);
    $('app-edit-doc').fadeOut(100);
    $('.edit-item-wrapper').removeClass('selected');
    $('.edit-item-wrapper').find('img').attr('src','./assets/img/blank_icon.png');
    this.selectedArray = [];
    $('div.deleteButton').removeClass('show');
  }

  selectedItem(event, docSubId:number){
    var item = event.currentTarget; 
    if ($(item).hasClass('selected')) {
      item.classList.remove('selected');
      $(item).find('img').attr('src','./assets/img/blank_icon.png');
      var index:number = this.selectedArray.indexOf(docSubId);
      if (index !== -1) {
        this.selectedArray.splice(index, 1);
      }
    } else {
      item.classList.add('selected');
      $(item).find('img').attr('src','./assets/img/checked_icon.png');
      this.selectedArray.push(docSubId);
    }
    if (this.selectedArray.length == 0) {
      $('div.deleteButton').removeClass('show');
    } else {
      $('div.deleteButton').addClass('show');
    }
  }
  
}
