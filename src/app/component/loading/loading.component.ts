import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.css']
})
export class LoadingComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    var top = (screen.height * 0.5) - 40;
    var left = (screen.width * 0.5) - 40;
    $('div.lds-eclipse').find('div').css('top', top+'px');
    $('div.lds-eclipse').find('div').css('left', left+'px');
  }

}
