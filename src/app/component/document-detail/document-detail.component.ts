import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Location } from '@angular/common';
import { DocumentDetailService } from './../../service/document-detail.service';
import { UserValueService } from './../../service/user-value.service';
import { DocumentAcceptService } from './../../service/document-accept.service';
import { ForwardListService } from './../../service/forward-list.service';
import { DoForwardService } from './../../service/do-forward.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-document-detail',
  templateUrl: './document-detail.component.html',
  styleUrls: ['./document-detail.component.css']
})
export class DocumentDetailComponent implements OnInit {

  documentDetail;
  docid;
  docsubid;
  isAccept;
  orderList;
  senderName;
  senderId;

  public token: string;
  public cmuaccount: string;

  acceptDocument;

  action: string = 'ตอบกลับ';
  actionCode: string = "REPLY";

  forwardResult;
  forwardList;
  tempLists: any[];
  lists: any[] = [];
  comment: string = '';

  leftBtn: string = 'ยกเลิก';
  rightBtn: string = 'ถัดไป';

  constructor(private _documentdetail: DocumentDetailService, private _route: ActivatedRoute, private _location: Location, private _uservalue: UserValueService, private _documentaccept: DocumentAcceptService, private _forwardList: ForwardListService, private _doforward: DoForwardService) {
      this._route.params
          .subscribe(
              (params: Params) => {
                  this.docid = params['docID'];
                  this.docsubid = params['docsubid'];
                  this.cmuaccount = params['cmuaccount'];
              }
          );
      this.getDocumentList('null');

  }

  ngOnInit() {
  }
  
  ngAfterViewInit() {
  }

  getDocumentList(key:string) {
    this._uservalue.getUserValue().subscribe(
        data => { 
            this.token = data.token;
            this.cmuaccount = data.cmuaccount;

            this._documentdetail.DocumentDetail(this.token, this.docid, this.docsubid, this.cmuaccount).subscribe(
                data => { 
                    this.documentDetail = data;
                    this.isAccept = this.documentDetail.data.accept;
                    this.orderList = this.documentDetail.data.orderModel.EdocTabOderDetailList;
                    this.senderName = this.documentDetail.data.orderModel.EdocTabOderDetailList[this.orderList.length-1].fromName;
                    this.senderId = this.documentDetail.data.orderModel.EdocTabOderDetailList[this.orderList.length-1].fromUserID;
                },
                err => console.error(err),
                () => {
                }
            );

        },
        err => console.error(err),
        () => {}
    );
  }

  openCity(tabbutton) {
    $('.tabcontent').removeClass('defaultopen');
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    $('#'+tabbutton).show();
    $('#'+tabbutton+'-btn').addClass('active');
  }
  
  onBack() {
    // this._location.back();
  }
  
  getUserValue() {
    this._uservalue.getUserValue().subscribe(
        data => { 
          this.token = data.token;
          this.cmuaccount = data.cmuaccount;
        },
        err => console.error(err),
        () => {}
    );
  }
  
// action
  actionAccept() {
    if(!this.isAccept) {
      this._documentaccept.acceptDocument(this.token, this.cmuaccount, this.docid, this.docsubid).subscribe(
          data => { 
            this.acceptDocument = data;
            this.isAccept = this.acceptDocument.data.accept;
          },
          err => console.error(err),
          () => { }
      );
    }
  }

  actionReply() {
    this.action = 'ตอบกลับ';
    this.actionCode = 'REPLY';
    this.lists = [{User_id:this.senderId, fullName:this.senderName}];
    $('div.app-forward').fadeIn(100);
  }

  actionForward() {
    this.action = 'ส่งต่อ';
    this.actionCode = 'FORWARD';
    this.lists = [];
    $('div.app-forward').fadeIn(100);
  }


  filterName(event) {
    let query = event.query;
    this._forwardList.getForwardList(this.token, this.cmuaccount, query).subscribe(
        data => { 
        this.forwardList = data;
        this.forwardList = this.forwardList.data;
        },
        err => console.error(err),
        () => { }
    );
  }

//   forward  
  onClickLeftButton(step){
    if ( step === 1 ) {
      $('div.app-forward').fadeOut(100);
      this.lists = [];
    } else {
      $('#comment-wrapper').fadeOut(100);
      this.leftBtn = "ยกเลิก";
      this.rightBtn = "ถัดไป";
    }
  }

  onClickRightButton(step){
    if ( step === 1 ) {
      $('#comment-wrapper').fadeIn(100);
      this.leftBtn = "ย้อนกลับ";
      this.rightBtn = this.action;
    } else {
      $('app-loading').show();
      var body = '{ "params" : { "data" : [';
      this.lists.forEach((list, index) => {
        body += '"' + list.User_id + '"';
        if ( index < this.lists.length-1 ) {
          body += ',';
        }
      });
      body += '] } }';
      this._doforward.doForward(this.actionCode, this.token, this.cmuaccount, this.docid, this.docsubid, this.comment, body).subscribe(
        data => {
          this.forwardResult = data;
          if ( this.forwardResult.status === 'pass' ) {
            location.href = "/";
          }
        },
        err => console.error(err),
        () => { }
      );
    }
  }

  removeFromList(item){
    const index: number = this.lists.indexOf(item);
    if (index !== -1) {
        this.lists.splice(index, 1);
    }       
  }

  listingName(event){
    var isDup = false;
    if (this.lists.length > 0) {
      this.lists.forEach(list => {
        if(list.User_id === event.User_id) {
          isDup = true;
        }
      });
    }
    if (!isDup) {
      this.lists.push(event);
    }
    this.tempLists = [];
    console.log(this.lists);
  }
}
