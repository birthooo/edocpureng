import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DocumentListService } from '../../service/document-list.service';
import { MobileTokenService } from '../../service/mobile-token.service';
import { UserValueService } from '../../service/user-value.service';
import { EditIsOpenService } from '../../service/edit-isOpen.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-document-list',
  templateUrl: './document-list.component.html',
  styleUrls: ['./document-list.component.css']
})
export class DocumentListComponent implements OnInit {

  searchTxt: string;
  doucumentList;
  display: boolean = false;
  
  public mobiletoken;
  public token: string;
  public cmuaccount: string;

  constructor(private _route: ActivatedRoute, private _documentlist:DocumentListService,
    private _mobiletoken: MobileTokenService, private _uservalue: UserValueService,
    private _editIsOpen: EditIsOpenService) {
      
    this._route.queryParams
        .subscribe(params => {
        this.token = params.token;
        this.cmuaccount = params.cmuaccount;
    });
  }

  ngOnInit() {
    // localStorage.clear();
    if(this.token === undefined && this.cmuaccount === undefined){
        if(localStorage.getItem('mobiletoken') === null){
          // No Param, No LS => Popup Re-login
          // console.log("No Param, No LS => Popup Re-login");
          this.display = true;
        }else{
            this.mobiletoken = localStorage.getItem('mobiletoken');
            this.token = JSON.parse(this.mobiletoken).token;
            this.cmuaccount = JSON.parse(this.mobiletoken).cmuaccount;
            this.getDocumentList('null');
        }
    }else{
        this.getMobileTokenData();
    }
  }

  topicSearch(){
      this.doucumentList = '';
      let search = 'null';
      if(this.searchTxt != undefined && this.searchTxt != ''){
          search = this.searchTxt;
      }
      this.getDocumentList(search);
  }
  
  getMobileTokenData() {
    this._mobiletoken.getMobileToken(this.token, this.cmuaccount).subscribe(
      data => {
        console.log(data);
        this.mobiletoken = data;
        if (this.mobiletoken.status == 'nopass') {
          // Popup Re-login
          this.display = true;
        } else {
          localStorage.setItem('mobiletoken', JSON.stringify(this.mobiletoken.data));
          this.mobiletoken = localStorage.getItem('mobiletoken');
          this.token = JSON.parse(this.mobiletoken).token;
          this.cmuaccount = JSON.parse(this.mobiletoken).cmuaccount;
          this.getDocumentList('null');
        }
      },
      err => console.error(err),
      () => { }
    );
  }

  getDocumentList(key:string) {
    this._documentlist.DocumentList(this.token, this.cmuaccount, key)
    .subscribe(
        data => { 
            this.doucumentList = data;
        },
        err => console.error(err),
        () => {
            this.setUserValue(this.token, this.cmuaccount);
            $('app-loading').fadeOut(100);
        }
    );
  }
  
  editIsOpen(docsubid:string) {
    this._editIsOpen.EditIsOpen(this.token, docsubid, this.cmuaccount)
    .subscribe(
      data => { },
      err => console.error(err),
      () => { }
    );
  }

  setUserValue(token: string, cmuaccount: string) {
    this._uservalue.setUserValue(token, cmuaccount);
  }

  openEdit() {
    $('app-edit-doc').fadeIn(100);
    $('.dim-background').fadeIn(100);
  }

  // showDialog() {
  //   this.display = true;
  // }
}
