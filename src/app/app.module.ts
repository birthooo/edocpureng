import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularFontAwesomeModule } from 'angular-font-awesome';

import { MobileTokenService } from './service/mobile-token.service';
import { DocumentListService } from './service/document-list.service';
import { DocumentDetailService } from './service/document-detail.service';
import { UserValueService } from './service/user-value.service';
import { EditIsOpenService } from './service/edit-isOpen.service';
import { DocumentDeleteService } from './service/documents-delete.service';
import { DocumentAcceptService } from './service/document-accept.service';
import { ForwardListService } from './service/forward-list.service';
import { DoForwardService } from './service/do-forward.service';

import { AppComponent } from './app.component';
import { DocumentListComponent } from './component/document-list/document-list.component';
import { DocumentDetailComponent } from './component/document-detail/document-detail.component';
import { LoadingComponent } from './component/loading/loading.component';
import { EditDocComponent } from './component/edit-doc/edit-doc.component';

import { AutoCompleteModule } from 'primeng/autocomplete';
import { DialogModule } from 'primeng/dialog';

const appRoutes:Routes = [
  {
    path: "",
    component: DocumentListComponent
  },
  {
    path: "document-list",
    component: DocumentListComponent
  },
  {
    path: "document-detail",
    component: DocumentDetailComponent
  },
  {
    path: "document-detail/:docID/:docsubid/:cmuaccount",
    component: DocumentDetailComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    DocumentListComponent,
    DocumentDetailComponent,
    LoadingComponent,
    EditDocComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AngularFontAwesomeModule,
    AutoCompleteModule,
    DialogModule
    
  ],
  providers: [
    MobileTokenService,
    DocumentListService,
    DocumentDetailService,
    UserValueService,
    EditIsOpenService,
    DocumentDeleteService,
    DocumentAcceptService,
    ForwardListService,
    DoForwardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
